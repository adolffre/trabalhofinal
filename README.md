# README #
The app that i developed to my Graduation thesis.
Cosiste in an app for high-performance athletes coaches that will help to know if yours athletes are rested for another training.



ABSTRACT

The lack of control in muscular recovery in high level athletes is a frequent problem that affects their performance competitions. This can cause muscular injuries that will keep the athlete away from practice for a long time, and so, compromising the athlete’s career. The objective of this work is to provide computational assistance to high level athlete’s coaches so that they can have greater control over the athlete training. So, it was verified through a specific questionnaire with athletes and physical educators, the res periods required for each muscle group. Through the diversity of rest intervals for each muscle group and each athlete, a mobile application has been developed aiming to help coaches with the selection of the muscular groups to be trained considering the recovery time of each specific athlete so that there is no muscular overload, and, consequently, obtain the best possible performance.
Key-words: High-level training, Muscle Recovery Intervals, Mobile Application