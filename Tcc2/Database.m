//
//  Database.m
//  Tcc2
//
//  Created by Adolf Jurgens on 17/08/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.

#import "Database.h"
#import "DatabaseUtils.h"

@implementation Database

static Database *_sharedInstance = nil;

+ (Database *)sharedInstance
{
    _sharedInstance = [[Database alloc] init];
    
    sqlite3 *database;
    
    if (sqlite3_open([[DatabaseUtils dbPath] UTF8String], &database) != SQLITE_OK)
        [[[NSException alloc] initWithName:@"Database error"
                                    reason:@"Não foi possível abrir o banco"
                                  userInfo:nil] raise];
    _sharedInstance.database = database;
    
    return _sharedInstance;
}

+ (sqlite3 *)sharedDatabase
{
    if (_sharedInstance == nil)
        _sharedInstance = [self sharedInstance];
    
    return _sharedInstance.database;
}

+ (sqlite3_stmt *)sharedStatement
{
    if (_sharedInstance == nil)
        _sharedInstance = [self sharedInstance];
    
    return _sharedInstance.statement;
}

+ (void)closeDatabase
{
    sqlite3_close(_sharedInstance.database);
}

+ (void)renewInstanceDatabase
{
    if (_sharedInstance && _sharedInstance.database) {
        sqlite3_close(_sharedInstance.database);
    }
    
    _sharedInstance = nil;
}

@end
