//
//  GrupoMuscularDB.m
//  Tcc2
//
//  Created by Adolf Jurgens on 17/08/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import "GrupoMuscularDB.h"

@implementation GrupoMuscularDB
- (id)init
{
    self = [super init];
    if (self) {
        _db = [Database sharedDatabase];
        _stmt = [Database sharedStatement];
    }
    return self;
}

+(NSArray *)getGruposMusculares{
    
    sqlite3 *db;
    sqlite3_stmt *stmt;
    db = [Database sharedDatabase];
    stmt = [Database sharedStatement];
    NSMutableArray *grupos = [NSMutableArray new];
    NSString *query = [[NSString alloc] initWithFormat:
                       @" SELECT codGrupoMuscular "
                       "    ,descricao "
                       "  FROM Grupo_Muscular "];
    
    const char *query_stmt = [query UTF8String];
    
    if (sqlite3_prepare(db, query_stmt, -1, &stmt, NULL) == SQLITE_OK)
    {
        while(sqlite3_step(stmt) == SQLITE_ROW)
        {
            GrupoMuscularDB *grupo = [GrupoMuscularDB new];
            
            grupo.code =  sqlite3_column_text(stmt, 0) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 0)] : nil;
            grupo.nome =  sqlite3_column_text(stmt, 1) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 1)] : nil;
            
            [grupos addObject:grupo];
        }
    }
    return  grupos;
    
}


@end
