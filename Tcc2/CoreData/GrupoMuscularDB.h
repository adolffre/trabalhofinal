//
//  GrupoMuscularDB.h
//  Tcc2
//
//  Created by Adolf Jurgens on 17/08/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Database.h"


@interface GrupoMuscularDB : NSObject

@property (nonatomic, strong)NSString *code;
@property (nonatomic, strong)NSString *nome;
@property (nonatomic) sqlite3 *db;
@property (nonatomic) sqlite3_stmt *stmt;

+(NSArray *)getGruposMusculares;


@end
