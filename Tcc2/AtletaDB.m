//
//  AtletaDB.m
//  Tcc2
//
//  Created by A. J. on 06/09/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import "AtletaDB.h"

@implementation AtletaDB


- (id)init
{
    self = [super init];
    if (self) {
        _db = [Database sharedDatabase];
        _stmt = [Database sharedStatement];
    }
    return self;
}
-(void)createTableIntervaloForAtleta:(AtletaDB *)atleta{
    
    NSString *query;

    NSMutableArray *periodos = [PeriodoDB getPeriodos];
    
    NSArray *homens = [[NSArray alloc] initWithObjects:@"6",@"3",@"3",@"6",@"3",@"5",@"3",@"5",@"3",@"5",@"3",@"5",@"3",@"7",@"4", nil];
    NSArray *mulheres = [[NSArray alloc] initWithObjects:@"3",@"1",@"1",@"3",@"1",@"3",@"1",@"3",@"1",@"3",@"1",@"3",@"2",@"3",@"2", nil];

    for (PeriodoDB *periodo in periodos) {
        NSArray *intervalos  = [atleta.sexo isEqualToString:@"M"]?homens:mulheres;
        int index = 0;
        NSString *codPediodo = periodo.code;
        
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"1",@"1",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"1",@"3",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"1",@"5",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"2",@"2",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"2",@"4",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"3",@"3",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"3",@"1",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"4",@"4",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"4",@"2",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"5",@"5",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"5",@"1",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"6",@"6",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"6",@"7",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"7",@"7",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"7",@"6",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
    }
    
}

- (void)save
{
    
    NSString *query = @"";
    self.code = [self createCodAtleta];
    
    query = [NSString stringWithFormat:
             @"INSERT INTO ATLETA "
             " ( "
             "  CodAtleta, "
             "  Nome, "
             "  Sexo, "
             "  Nascimento "
             " ) "
             " values('%@', '%@', '%@', '%@') "
             " ; "
             , self.code
             , self.nome
             , self.sexo
             , self.dataNascimento
             ];
    char *errMsg = NULL;
    
    
    if (sqlite3_exec(self.db, "BEGIN", 0, 0, 0) == SQLITE_OK)
    {
        
        int execResult = sqlite3_exec(self.db, [query UTF8String], NULL, NULL, &errMsg);
        
        if (execResult != SQLITE_OK)
        {
            sqlite3_exec(self.db, "ROLLBACK", 0, 0, 0);
            
            NSString *errorMsg = [NSString stringWithFormat:
                                  @"Falha no sqlite3_exec na classe %@;\n"
                                  "método save;\n"
                                  "sqlite error code : %i;\n"
                                  "error msg: %s"
                                  "SQL: %@", self.class ,execResult, errMsg, query];
            
            
            ALog(@"%@", errorMsg);
            
        }
        
        sqlite3_exec(self.db, "COMMIT", 0, 0, 0);
        [self createTableIntervaloForAtleta:self];
    }
    else
    {
        NSString *errorMsg = [NSString stringWithFormat:
                              @"Falha no sqlite3_exec na classe %@;\n"
                              "método save;\n"
                              "BEGIN TRANSACTION ERROR", self.class];
        
        
        ALog(@"%@", errorMsg);
        
        
    }
}

- (NSString *)createCodAtleta
{
    int ultimoCodAtleta = 0;
    
    NSString *query = [[NSString alloc] initWithFormat:
                       @" SELECT MAX( "
                       "  CodAtleta) "
                       "  FROM Atleta "];
    
    const char *query_stmt = [query UTF8String];
    
    if (sqlite3_prepare(_db, query_stmt, -1, &_stmt, NULL) == SQLITE_OK)
    {
        if (sqlite3_step(_stmt) == SQLITE_ROW)
        {
            ultimoCodAtleta = sqlite3_column_int(_stmt, 0);
        }
    }
    
    return [NSString stringWithFormat:@"%i",ultimoCodAtleta + 1];
}

+(NSMutableArray *)getAtletas{
    
   sqlite3 *db;
   sqlite3_stmt *stmt;
    db = [Database sharedDatabase];
    stmt = [Database sharedStatement];
    NSMutableArray *atletas = [NSMutableArray new];
    NSString *query = [[NSString alloc] initWithFormat:
                       @" SELECT codAtleta "
                       "    ,Nome "
                       "    ,Sexo "
                       "    ,Nascimento "
                       "  FROM Atleta "];
    
    const char *query_stmt = [query UTF8String];
    
    if (sqlite3_prepare(db, query_stmt, -1, &stmt, NULL) == SQLITE_OK)
    {
        while(sqlite3_step(stmt) == SQLITE_ROW)
        {
            AtletaDB *atleta = [AtletaDB new];
            
            atleta.code =  sqlite3_column_text(stmt, 0) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 0)] : nil;
            atleta.nome =  sqlite3_column_text(stmt, 1) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 1)] : nil;
            
            atleta.sexo =  sqlite3_column_text(stmt, 2) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 2)] : nil;
            atleta.dataNascimento = sqlite3_column_text(stmt, 3) ? [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 3)] toDate] : nil;
            
            [atletas addObject:atleta];
        }
    }
    return  atletas;
    
}
- (BOOL)remove
{
    NSString *query = [NSString stringWithFormat:
                       @" DELETE FROM TREINO "
                       " WHERE CodAtleta = '%@'; "
                       , self.code];
    
    
    query = [query stringByAppendingString:[NSString stringWithFormat:
                                            @" DELETE FROM ATLETA "
                                            " WHERE CodAtleta = '%@'; "
                                            , self.code]];
    query = [query stringByAppendingString:[NSString stringWithFormat:
                                            @" DELETE FROM INTERVALO_TREINO "
                                            " WHERE CodAtleta = '%@'; "
                                            , self.code]];

    char *errMsg = NULL;
    
    if (sqlite3_exec(self.db, "BEGIN", 0, 0, 0) == SQLITE_OK)
    {
        
        int execResult = sqlite3_exec(self.db, [query UTF8String], NULL, NULL, &errMsg);
        
        if (execResult != SQLITE_OK)
        {
            sqlite3_exec(self.db, "ROLLBACK", 0, 0, 0);
            
            NSString *errorMsg = [NSString stringWithFormat:
                                  @"Falha no sqlite3_exec na classe %@;\n"
                                  "método remove;\n"
                                  "sqlite error code : %i;\n"
                                  "error msg: %s"
                                  "SQL: %@", self.class ,execResult, errMsg, query];
            
            
            ALog(@"%@", errorMsg);
            
            return NO;
        }
        
        sqlite3_exec(self.db, "COMMIT", 0, 0, 0);
    }
    else
    {
        NSString *errorMsg = [NSString stringWithFormat:
                              @"Falha no sqlite3_exec na classe %@;\n"
                              "método remove;\n"
                              "BEGIN TRANSACTION ERROR", self.class];
        
        ALog(@"%@", errorMsg);
        
        return YES;
    }
    
    return YES;
}

- (int) runSQLCommand:(NSString *)sqlCommand withErrorLog:(BOOL)withErrorLog
{
    NSString *sql;
    char * errMsg = NULL;
    
    int prepareResult = sqlite3_exec(_db, [sqlCommand UTF8String], NULL, NULL, &errMsg);
    
    if (prepareResult != SQLITE_OK)
    {
        NSString *errorMsg = [NSString stringWithFormat:
                              @"Falha no sqlite3_exec na classe %@;\n"
                              "método limpaDados;\n"
                              "sqlite error code : %i;\n"
                              "errMsg : %s;\n"
                              "SQL: %@", self.class ,prepareResult, errMsg , sql];
        
        if (withErrorLog)
            ALog(@"%@", errorMsg);
        
        return prepareResult;
    }
    
    return 0;
}
@end
