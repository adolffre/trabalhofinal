//
//  Created by jve on 4/1/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "Parameters.h"
@class DragContext, DragDropManager;


@protocol DragDropManagerDelegate <NSObject>
-(void)dropGrupo:(UIView*)grupoMuscular onDia:(UIView *)diaSemana;
-(void)didFinishedDragDrop;

@end
@interface DragDropManager : NSObject
- (id)initWithDragSubjects:(NSArray *)dragSubjects andDropAreas:(NSArray *)dropAreas;

- (void)dragging:(id)sender;


@property(nonatomic, retain) DragContext *dragContext;
@property(nonatomic, retain, readonly) NSArray *dropAreas;
@property (weak, nonatomic) id <DragDropManagerDelegate> delegate;

@end