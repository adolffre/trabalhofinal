//
//  TreinoDB.m
//  Tcc2
//
//  Created by A. J. on 06/09/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import "TreinoDB.h"
#import "FormatCategories.h"

@implementation TreinoDB
- (id)init
{
    self = [super init];
    if (self) {
        _db = [Database sharedDatabase];
        _stmt = [Database sharedStatement];
    }
    return self;
}
+(NSMutableArray *)getTreinosFromAtleta:(AtletaDB *)atleta inPeriodo:(PeriodoDB *)periodo atSemana:(NSDate *)dataInicio{
    
    sqlite3 *db;
    sqlite3_stmt *stmt;
    db = [Database sharedDatabase];
    stmt = [Database sharedStatement];
    NSMutableArray *treinosSemana = [NSMutableArray new];
    
    NSDate *dataFim = [dataInicio addDays:7];

    
    
//    int diaMais = 7;
//    NSDate *dataFim = [[dataInicio dateByAddingTimeInterval:60*60*24*diaMais] dateAtMidnight];
//    
    
    
    NSString *query = [[NSString alloc] initWithFormat:
                       @" SELECT T.CodTreino "
                       "        ,T.CodAtleta "
                       "        ,T.CodPeriodo "
                       "        ,T.CodGrupoMuscular "
                       "        ,T.DataTreino "
                       "  FROM TREINO T "
                       "WHERE T.CodAtleta = '%@' "
                       "        AND T.CodPeriodo = '%@' "
                       "        AND T.DataTreino >= '%@' "
                       "        AND T.DataTreino <= '%@' "
                       ,atleta.code
                       ,periodo.code
                       ,[dataInicio toDbString]
                       ,[dataFim toDbString]];
    
    const char *query_stmt = [query UTF8String];
    
    if (sqlite3_prepare(db, query_stmt, -1, &stmt, NULL) == SQLITE_OK)
    {
        while(sqlite3_step(stmt) == SQLITE_ROW)
        {
            TreinoDB *treino = [TreinoDB new];
            
            treino.code =  sqlite3_column_text(stmt, 0) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 0)] : nil;
            treino.codAtleta =  sqlite3_column_text(stmt, 1) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 1)] : nil;
             treino.codPeriodo =  sqlite3_column_text(stmt, 2) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 2)] : nil;
             treino.codGrupoMuscular =  sqlite3_column_text(stmt, 3) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 3)] : nil;
             treino.dataTreino = sqlite3_column_text(stmt, 4) ? [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 4)] toDate] : nil;
            
            
            [treinosSemana addObject:treino];
        }
    }
    return  treinosSemana;
    
}
- (BOOL)remove
{
    NSString *query = [NSString stringWithFormat:
                       @"DELETE FROM TREINO "
                       " WHERE Codtreino = '%@' "
                       "    AND CodAtleta = '%@' "
                       "    AND CodPeriodo = '%@' "
                       "    AND CodGrupoMuscular = '%@' "
                       , self.code
                       , self.codAtleta
                       , self.codPeriodo
                       , self.codGrupoMuscular];
    
    char *errMsg = NULL;
    
    if (sqlite3_exec(self.db, "BEGIN", 0, 0, 0) == SQLITE_OK)
    {
        
        int execResult = sqlite3_exec(self.db, [query UTF8String], NULL, NULL, &errMsg);
        
        if (execResult != SQLITE_OK)
        {
            sqlite3_exec(self.db, "ROLLBACK", 0, 0, 0);
            
            NSString *errorMsg = [NSString stringWithFormat:
                                  @"Falha no sqlite3_exec na classe %@;\n"
                                  "método remove;\n"
                                  "sqlite error code : %i;\n"
                                  "error msg: %s"
                                  "SQL: %@", self.class ,execResult, errMsg, query];
            
            
            ALog(@"%@", errorMsg);
            
            return NO;
        }
        
        sqlite3_exec(self.db, "COMMIT", 0, 0, 0);
    }
    else
    {
        NSString *errorMsg = [NSString stringWithFormat:
                              @"Falha no sqlite3_exec na classe %@;\n"
                              "método remove;\n"
                              "BEGIN TRANSACTION ERROR", self.class];
        
        ALog(@"%@", errorMsg);
        
        return YES;
    }
    
    return YES;
}
- (void)save
{
    
    NSString *query = @"";
    self.code = [self createCodTreino];
    
    query = [NSString stringWithFormat:
             @"INSERT INTO TREINO "
             " ( "
             "  CodTreino, "
             "  CodAtleta, "
             "  CodPeriodo, "
             "  CodGrupoMuscular, "
             "  DataTreino "
             " ) "
             " values('%@', '%@', '%@', '%@', '%@') "
             " ; "
             , self.code
             , self.codAtleta
             , self.codPeriodo
             , self.codGrupoMuscular
             , [self.dataTreino toDbString]
             ];
    char *errMsg = NULL;
    
    
    if (sqlite3_exec(self.db, "BEGIN", 0, 0, 0) == SQLITE_OK)
    {
        
        int execResult = sqlite3_exec(self.db, [query UTF8String], NULL, NULL, &errMsg);
        
        if (execResult != SQLITE_OK)
        {
            sqlite3_exec(self.db, "ROLLBACK", 0, 0, 0);
            
            NSString *errorMsg = [NSString stringWithFormat:
                                  @"Falha no sqlite3_exec na classe %@;\n"
                                  "método save;\n"
                                  "sqlite error code : %i;\n"
                                  "error msg: %s"
                                  "SQL: %@", self.class ,execResult, errMsg, query];
            
            
            ALog(@"%@", errorMsg);
            
        }
        
        sqlite3_exec(self.db, "COMMIT", 0, 0, 0);
    }
    else
    {
        NSString *errorMsg = [NSString stringWithFormat:
                              @"Falha no sqlite3_exec na classe %@;\n"
                              "método save;\n"
                              "BEGIN TRANSACTION ERROR", self.class];
        
        
        ALog(@"%@", errorMsg);
        
        
    }
}

- (NSString *)createCodTreino
{
    int ultimoCodTreino = 0;
    
    NSString *query = [[NSString alloc] initWithFormat:
                       @" SELECT MAX( "
                       "  CodTreino) "
                       "  FROM Treino "];
    
    const char *query_stmt = [query UTF8String];
    
    if (sqlite3_prepare(_db, query_stmt, -1, &_stmt, NULL) == SQLITE_OK)
    {
        if (sqlite3_step(_stmt) == SQLITE_ROW)
        {
            ultimoCodTreino = sqlite3_column_int(_stmt, 0);
        }
    }
    
    return [NSString stringWithFormat:@"%i",ultimoCodTreino + 1];
}
-(TreinoDB *)getLastTreinoWithCodGrupoMuscular:(NSString *)codGrupoMuscular{
    TreinoDB *treino = [TreinoDB new];
    sqlite3 *db;
    sqlite3_stmt *stmt;
    db = [Database sharedDatabase];
    stmt = [Database sharedStatement];
    
    NSString *query = [[NSString alloc] initWithFormat:
                       @" SELECT T.CodTreino "
                       "        ,T.CodAtleta "
                       "        ,T.CodPeriodo "
                       "        ,T.CodGrupoMuscular "
                       "        ,T.DataTreino "
                       "  FROM TREINO T "
                       "WHERE T.CodAtleta = '%@' "
                       "        AND T.CodPeriodo = '%@' "
                       "        AND T.CodGrupoMuscular = '%@' "
                       "ORDER BY T.DataTreino DESC LIMIT 1 "
                       ,[Parameters sharedParameters].atleta.code
                       ,[Parameters sharedParameters].periodo.code
                       ,codGrupoMuscular];
    
    const char *query_stmt = [query UTF8String];
    
    if (sqlite3_prepare(db, query_stmt, -1, &stmt, NULL) == SQLITE_OK)
    {
        if(sqlite3_step(stmt) == SQLITE_ROW)
        {
            treino.code =  sqlite3_column_text(stmt, 0) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 0)] : nil;
            treino.codAtleta =  sqlite3_column_text(stmt, 1) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 1)] : nil;
            treino.codPeriodo =  sqlite3_column_text(stmt, 2) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 2)] : nil;
            treino.codGrupoMuscular =  sqlite3_column_text(stmt, 3) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 3)] : nil;
            treino.dataTreino = sqlite3_column_text(stmt, 4) ? [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 4)] toDate] : nil;
        }
    }
    return treino;
}


+(NSArray *)getTreinosWithCodGrupoMuscular:(NSString *)codGrupoMuscular{
    sqlite3 *db;
    sqlite3_stmt *stmt;
    db = [Database sharedDatabase];
    stmt = [Database sharedStatement];
    NSMutableArray * treinos = [NSMutableArray new];
    NSString *query = [[NSString alloc] initWithFormat:
                       @" SELECT T.CodTreino "
                       "        ,T.CodAtleta "
                       "        ,T.CodPeriodo "
                       "        ,T.CodGrupoMuscular "
                       "        ,T.DataTreino "
                       "  FROM TREINO T "
                       "WHERE T.CodAtleta = '%@' "
                       "        AND T.CodPeriodo = '%@' "
                       "        AND T.CodGrupoMuscular = '%@' "
                       "ORDER BY T.DataTreino  "
                       ,[Parameters sharedParameters].atleta.code
                       ,[Parameters sharedParameters].periodo.code
                       ,codGrupoMuscular];
    
    const char *query_stmt = [query UTF8String];
    
    if (sqlite3_prepare(db, query_stmt, -1, &stmt, NULL) == SQLITE_OK)
    {
        while(sqlite3_step(stmt) == SQLITE_ROW)
        {
            TreinoDB *treino = [TreinoDB new];
        
            treino.code =  sqlite3_column_text(stmt, 0) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 0)] : nil;
            treino.codAtleta =  sqlite3_column_text(stmt, 1) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 1)] : nil;
            treino.codPeriodo =  sqlite3_column_text(stmt, 2) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 2)] : nil;
            treino.codGrupoMuscular =  sqlite3_column_text(stmt, 3) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 3)] : nil;
            treino.dataTreino = sqlite3_column_text(stmt, 4) ? [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 4)] toDate] : nil;
            
            [treinos addObject:treino];
        }
    }
    return [NSArray arrayWithArray:treinos];
}

+(NSArray *)getTreinosWithCodGrupoMuscular1:(NSString *)codGrupoMuscular1 andCodMuscular2:(NSString *)codGrupoMuscular2{
    sqlite3 *db;
    sqlite3_stmt *stmt;
    db = [Database sharedDatabase];
    stmt = [Database sharedStatement];
    NSMutableArray * treinos = [NSMutableArray new];
    NSString *query = [[NSString alloc] initWithFormat:
                       @" SELECT T.CodTreino "
                       "        ,T.CodAtleta "
                       "        ,T.CodPeriodo "
                       "        ,T.CodGrupoMuscular "
                       "        ,T.DataTreino "
                       "  FROM TREINO T "
                       "WHERE T.CodAtleta = '%@' "
                       "        AND T.CodPeriodo = '%@' "
                       "        AND T.CodGrupoMuscular IN ('%@','%@') "
                       "ORDER BY T.DataTreino  "
                       ,[Parameters sharedParameters].atleta.code
                       ,[Parameters sharedParameters].periodo.code
                       ,codGrupoMuscular1
                       ,codGrupoMuscular2];
    
    const char *query_stmt = [query UTF8String];
    
    if (sqlite3_prepare(db, query_stmt, -1, &stmt, NULL) == SQLITE_OK)
    {
        while(sqlite3_step(stmt) == SQLITE_ROW)
        {
            TreinoDB *treino = [TreinoDB new];
            
            treino.code =  sqlite3_column_text(stmt, 0) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 0)] : nil;
            treino.codAtleta =  sqlite3_column_text(stmt, 1) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 1)] : nil;
            treino.codPeriodo =  sqlite3_column_text(stmt, 2) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 2)] : nil;
            treino.codGrupoMuscular =  sqlite3_column_text(stmt, 3) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 3)] : nil;
            treino.dataTreino = sqlite3_column_text(stmt, 4) ? [[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 4)] toDate] : nil;
            
            [treinos addObject:treino];
        }
    }
    return [NSArray arrayWithArray:treinos];
}

@end
