//
//  GrupoMuscular.m
//  Tcc2
//
//  Created by Adolf Jurgens on 17/08/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import "GrupoMuscular.h"

@implementation GrupoMuscular

- (id)initWithGrupo:(GrupoMuscularDB*)grupo andOffSet:(int)offSet
{
    int w = 135;
    int h = 87;
    self = [super initWithFrame:CGRectMake(offSet, 5, w, h)];
    
    NSDictionary *dicColors = [[NSDictionary alloc] initWithObjectsAndKeys:
                               @"#196728",@"1" ,
                               @"#1b6e2b",@"2" ,
                               @"#1d762e",@"3" ,
                               @"#1f7d31",@"4" ,
                               @"#208433",@"5" ,
                               @"#228c36",@"6" ,
                               @"#249339",@"7" 
                               , nil];
    if (self) {
        _lblGrupName = [[UILabel alloc] initWithFrame:CGRectMake(0, 28, w, 31)];
        _codGrupo = grupo.code;
        [_lblGrupName setText:grupo.nome];
        [_lblGrupName setTextAlignment:NSTextAlignmentCenter];
        [_lblGrupName setTextColor:[UIColor whiteColor]];
        [self setBackgroundColor:[UIColor colorWithHexString:[dicColors objectForKey:_codGrupo]]];
        self.layer.cornerRadius = 8;
        self.layer.masksToBounds = YES;
        
        
        [self addSubview:_lblGrupName];
        //[self setBackgroundColor:[UIColor redColor]];
    }
    return self;
}
- (id)initWithGrupo:(GrupoMuscularDB*)grupo andIndexDia:(int)index
{
    int w = 135;
    int h = 87;
    int y = 90 *index;
    y = index>0?y:y;
    self = [super initWithFrame:CGRectMake(3, y+44, w, h)];
    NSDictionary *dicColors = [[NSDictionary alloc] initWithObjectsAndKeys:
                               @"#196728",@"1" ,
                               @"#1b6e2b",@"2" ,
                               @"#1d762e",@"3" ,
                               @"#1f7d31",@"4" ,
                               @"#208433",@"5" ,
                               @"#228c36",@"6" ,
                               @"#249339",@"7"
                               , nil];

    if (self) {
        _lblGrupName = [[UILabel alloc] initWithFrame:CGRectMake(0, 28, w, 31)];
        _codGrupo = grupo.code;
        [_lblGrupName setText:grupo.nome];
        [_lblGrupName setTextAlignment:NSTextAlignmentCenter];
        [_lblGrupName setTextColor:[UIColor whiteColor]];
        
        self.layer.cornerRadius = 8;
        self.layer.masksToBounds = YES;
        
        
        [self addSubview:_lblGrupName];
         [self setBackgroundColor:[UIColor colorWithHexString:[dicColors objectForKey:_codGrupo]]];
    }
    return self;
}
@end
