//
//  Database.h
//  Tcc2
//
//  Created by Adolf Jurgens on 17/08/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "sqlite3.h"

/**
 Classe com métodos para acessar uma instancia única do banco
 Deve ser usada em todos os locais da aplicação que usa sqlite
 **/
@interface Database : NSObject

@property (nonatomic) sqlite3_stmt *statement;

@property (nonatomic) sqlite3 *database;

+ (sqlite3 *)sharedDatabase;

+ (sqlite3_stmt *)sharedStatement;

+ (void)closeDatabase;

+ (void)renewInstanceDatabase;

@end
