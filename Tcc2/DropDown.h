//
//  DropDown.h
//  DropDown
//
//  Created by Bijesh N on 12/28/12.
//  Copyright (c) 2012 Nitor Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AtletaDB.h"
#import "PeriodoDB.h"

@class DropDown;
@protocol dropDownDelegate
- (void) dropDownDelegateMethod: (UIButton *)sender andIndex:(int)index;
@end

@interface DropDown : UIView <UITableViewDelegate, UITableViewDataSource>
{
    NSString *animationDirection;
    UIImageView *imgView;
}
@property (nonatomic, retain) id <dropDownDelegate> delegate;
@property (nonatomic, retain) NSString *animationDirection;
-(void)hideDropDown:(UIButton *)button;
- (id)showDropDown:(UIButton *)button withArray:(NSArray *)arr andDirection:(NSString *)direction;
@end
