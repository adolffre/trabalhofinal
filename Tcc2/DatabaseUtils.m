//
//  DatabaseUtils.m
//  Tcc2
//
//  Created by Adolf Jurgens on 17/08/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.


#import "DatabaseUtils.h"
#import "Database.h"

@implementation DatabaseUtils

+ (NSString *)dbPath
{
    NSString *dbName;
    
        dbName =  @"BancoLocal";
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [NSString stringWithFormat:@"%@/%@.sqlite", [paths objectAtIndex:0], dbName];
    
    return path;
}

+ (void)copyDatabase
{
    NSString *dbName;
    
        dbName = @"BancoLocal";
    
    NSString *dbPath = [[NSBundle mainBundle] pathForResource:dbName ofType:@"sqlite"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [NSString stringWithFormat:@"%@/%@.sqlite", [paths objectAtIndex:0], dbName];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:path])
    {
        NSURL *urlFile = [[NSURL alloc] initFileURLWithPath:path];
        
        NSError *error = nil;
        
        [fileManager copyItemAtPath:dbPath toPath:path error:&error];
        
        [urlFile setResourceValue: [NSNumber numberWithBool: YES] forKey:
         NSURLIsExcludedFromBackupKey error: &error];
        
        if (error)
            [[[NSException alloc] initWithName:@"File error"
                                        reason:@"erro ao copiar banco"
                                      userInfo:nil] raise];
    }
    
}


+ (void)dropCurrentDatabase
{
    NSString *filePath = [DatabaseUtils dbPath];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }
    
    [Database renewInstanceDatabase];
    
}

@end
