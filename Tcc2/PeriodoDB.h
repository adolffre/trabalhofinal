//
//  PeriodoDB.h
//  Tcc2
//
//  Created by A. J. on 06/09/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Database.h"
#import "AtletaDB.h"

@interface PeriodoDB : NSObject
@property (nonatomic, strong)NSString *code;
@property (nonatomic, strong)NSString *nome;
@property (nonatomic) sqlite3 *db;
@property (nonatomic) sqlite3_stmt *stmt;
- (void)save;
- (BOOL)remove;
+(NSMutableArray *)getPeriodos;
@end
