//
//  AtletaDB.h
//  Tcc2
//
//  Created by A. J. on 06/09/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Database.h"
#import "FormatCategories.h"
#import "PeriodoDB.h"
@interface AtletaDB : NSObject
@property (nonatomic, strong)NSString *code;
@property (nonatomic, strong)NSString *nome;
@property (nonatomic, strong)NSString *sexo;
@property (nonatomic, strong)NSDate *dataNascimento;
@property (nonatomic) sqlite3 *db;
@property (nonatomic) sqlite3_stmt *stmt;
- (void)save;
- (BOOL)remove;
- (int) runSQLCommand:(NSString *)sqlCommand withErrorLog:(BOOL)withErrorLog;
+(NSMutableArray *)getAtletas;
@end
