//
//  Created by jve on 4/1/12.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "DragDropManager.h"
#import "DragContext.h"



@implementation DragDropManager {

    NSArray *_dragSubjects;
    NSArray *_dropAreas;
    DragContext *_dragContext;
}

@synthesize dragContext = _dragContext;
@synthesize dropAreas = _dropAreas;


- (id)initWithDragSubjects:(NSArray *)dragSubjects andDropAreas:(NSArray *)dropAreas {
    self = [super init];
    if (self) {
        _dropAreas = dropAreas ;
        _dragSubjects = dragSubjects;
        _dragContext = nil;
    }

    return self;
}


- (void)dragObjectAccordingToGesture:(UIPanGestureRecognizer *)recognizer {
    if (self.dragContext) {
        CGPoint pointOnView = [recognizer locationInView:recognizer.view];
        self.dragContext.draggedView.center = pointOnView;
    }
}

- (void)dragging:(id)sender {
    UIPanGestureRecognizer *recognizer = (UIPanGestureRecognizer *) sender;
    if( ![Parameters sharedParameters].atleta ||  ![Parameters sharedParameters].periodo){
        if(recognizer.state == UIGestureRecognizerStateBegan){
        
            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Atenção" message:@"É necessário estar com um atleta e um periodo selecionado." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else{
        BOOL droppedViewInKnownArea = NO;
        UIPanGestureRecognizer *recognizer = (UIPanGestureRecognizer *) sender;
        if(recognizer.state == UIGestureRecognizerStateBegan) {
            for (UIView *dragSubject in _dragSubjects) {
                //todo: pointInside seems to answer no even tough the point is actually inside the view?
                CGPoint pointInSubjectsView = [recognizer locationInView:dragSubject];
                BOOL pointInSideDraggableObject = [dragSubject pointInside:pointInSubjectsView withEvent:nil];
                //NSLog(@"point%@ %@ subject%@", NSStringFromCGPoint(pointInSubjectsView), pointInSideDraggableObject ? @"inside" : @"outside", NSStringFromCGRect(dragSubject.frame));
                if (pointInSideDraggableObject) {
                    //NSLog(@"started dragging an object");
                    self.dragContext = [[DragContext alloc] initWithDraggedView:dragSubject];
                    [dragSubject removeFromSuperview];
                    [recognizer.view addSubview:dragSubject];
                    [self dragObjectAccordingToGesture:recognizer];
                } else {
                    //NSLog(@"started drag outside drag subjects");
                }
            }

        }
        else if(recognizer.state == UIGestureRecognizerStateChanged) {

            [self dragObjectAccordingToGesture:recognizer];
        }
        else if(recognizer.state == UIGestureRecognizerStateEnded) {

            if (self.dragContext) {
                UIView *viewBeingDragged = self.dragContext.draggedView;
                //NSLog(@"ended drag event");
                CGPoint centerOfDraggedView = viewBeingDragged.center;
                droppedViewInKnownArea = NO;
                //   for(int i = 0; i < [self.dropAreas count]; i++) {
                //     UIView *dropArea = [self.dropAreas objectAtIndex:i];
                for (UIView *dropArea in self.dropAreas) {
                    CGPoint pointInDropView = [recognizer locationInView:dropArea];
                    //NSLog(@"tag %d pointInDropView %@ center of dragged view %@", dropArea.tag, NSStringFromCGPoint(pointInDropView), NSStringFromCGPoint(centerOfDraggedView));
                    if ([dropArea pointInside:pointInDropView withEvent:nil]) {
                        droppedViewInKnownArea = YES;
                      //  NSLog(@"dropped subject %@ on to view tag %d", NSStringFromCGRect(viewBeingDragged.frame), dropArea.tag);
                        [viewBeingDragged removeFromSuperview];
    //                        CGRect frame = CGRectMake(5, 40, viewBeingDragged.frame.size.width, viewBeingDragged.frame.size.height);
    //                        [viewBeingDragged setFrame:frame];
    //
                        [dropArea addSubview:viewBeingDragged];
                        [self.delegate dropGrupo:viewBeingDragged onDia:dropArea];

                        //change origin to match offset on new super view
                        viewBeingDragged.frame = CGRectMake(pointInDropView.x - (viewBeingDragged.frame.size.width / 2), pointInDropView.y - (viewBeingDragged.frame.size.height / 2), viewBeingDragged.frame.size.width, viewBeingDragged.frame.size.height);
                        
                    }
                }

                if (!droppedViewInKnownArea) {
                    //NSLog(@"release draggable object outside target views - snapping back to last known location");
                    [self.dragContext snapToOriginalPosition];
                }
                else{
                     [self.delegate didFinishedDragDrop];
                }

                self.dragContext = nil;
            } else {
               // NSLog(@"Nothing was being dragged");
            }
        }
    }
    
}
@end