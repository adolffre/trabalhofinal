//
//  TreinoDB.h
//  Tcc2
//
//  Created by A. J. on 06/09/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Database.h"
#import "AtletaDB.h"
#import "GrupoMuscularDB.h"
#import "PeriodoDB.h"
#import "Parameters.h"

@interface TreinoDB : NSObject


@property (nonatomic, strong)NSString *code;
@property (nonatomic, strong)NSString *codAtleta;
@property (nonatomic, strong)NSString *codPeriodo;
@property (nonatomic, strong)NSString *codGrupoMuscular;
@property (nonatomic, strong)NSDate *dataTreino;

@property (nonatomic) sqlite3 *db;
@property (nonatomic) sqlite3_stmt *stmt;
+(NSMutableArray *)getTreinosFromAtleta:(AtletaDB *)atleta inPeriodo:(PeriodoDB *)periodo atSemana:(NSDate *)dataInicio;
+(NSArray *)getTreinosWithCodGrupoMuscular:(NSString *)codGrupoMuscular;
-(TreinoDB *)getLastTreinoWithCodGrupoMuscular:(NSString *)codGrupoMuscular;
+(NSArray *)getTreinosWithCodGrupoMuscular1:(NSString *)codGrupoMuscular1 andCodMuscular2:(NSString *)codGrupoMuscular2;

- (BOOL)remove;
- (void)save;
@end
