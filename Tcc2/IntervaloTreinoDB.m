//
//  IntervaloTreinoDB.m
//  Tcc2
//
//  Created by A. J. on 28/09/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import "IntervaloTreinoDB.h"

@implementation IntervaloTreinoDB

+(NSArray *)intervalosFromCodGrupoMuscular:(NSString *)codGrupoMuscular withAtleta: (AtletaDB *)atleta andPeriodo:(PeriodoDB *)periodo {
    sqlite3 *db;
    sqlite3_stmt *stmt;
    db = [Database sharedDatabase];
    stmt = [Database sharedStatement];
    NSMutableArray *intervalos = [NSMutableArray new];
    
    NSString *query = [[NSString alloc] initWithFormat:
                       @" SELECT IT.CodGrupoMuscular1 "
                       "        ,IT.CodGrupoMuscular2 "
                       "        ,IT.CodAtleta "
                       "        ,IT.CodPeriodo "
                       "        ,IT.Intervalo "
                       "  FROM INTERVALO_TREINO IT "
                       "  WHERE IT.CodGrupoMuscular1 = '%@' "
                       "  AND   IT.CodAtleta = '%@' "
                       "  AND   IT.CodPeriodo = '%@' "
                       "ORDER BY rowid "
                        ,codGrupoMuscular
                        ,atleta.code
                        ,periodo.code ];
    
    const char *query_stmt = [query UTF8String];
    
    if (sqlite3_prepare(db, query_stmt, -1, &stmt, NULL) == SQLITE_OK)
    {
        while (sqlite3_step(stmt) == SQLITE_ROW)
        {
            IntervaloTreinoDB *intervaloTreino = [IntervaloTreinoDB new];
            intervaloTreino.codGrupoMuscular1 =  sqlite3_column_text(stmt, 0) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 0)] : nil;
            intervaloTreino.codGrupoMuscular2 =  sqlite3_column_text(stmt, 1) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 1)] : nil;
            intervaloTreino.codAtleta =  sqlite3_column_text(stmt, 2) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 2)] : nil;
            intervaloTreino.codPeriodo =  sqlite3_column_text(stmt, 3) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 3)] : nil;
            intervaloTreino.intervalo =  sqlite3_column_int(stmt, 4);
            
            [intervalos addObject:intervaloTreino];
        }
    }
    return intervalos;
}
+(NSString *)validaPossibilidadeTreino:(NSArray *)intervalosGrupoSelecionado eDataTreino:(NSDate *)dataTreino{
    sqlite3 *db;
    sqlite3_stmt *stmt;
    db = [Database sharedDatabase];
    stmt = [Database sharedStatement];
    NSString *retorno =@"";
    for (int i =0; i<intervalosGrupoSelecionado.count; i++) {
    
        IntervaloTreinoDB *intervalorG = (IntervaloTreinoDB *)[intervalosGrupoSelecionado objectAtIndex:i];
        NSString *query = [[NSString alloc] initWithFormat:
                           @" SELECT T.DataTreino "
                           "  ,GM.Descricao "
                           "  FROM TREINO T "
                           "  INNER JOIN GRUPO_MUSCULAR GM ON GM.codGrupoMuscular = T.codGrupoMuscular "
                           "  WHERE T.CodAtleta = '%@' "
                           "        AND T.CodPeriodo = '%@' "
                           "        AND T.CodGrupoMuscular = '%@' "
                           "  ORDER BY DATATREINO DESC LIMIT 1 "
                           ,[Parameters sharedParameters].atleta.code
                           ,[Parameters sharedParameters].periodo.code
                           ,[intervalorG codGrupoMuscular2] ];
        
        const char *query_stmt = [query UTF8String];
        
        if (sqlite3_prepare(db, query_stmt, -1, &stmt, NULL) == SQLITE_OK)
        {
            if (sqlite3_step(stmt) == SQLITE_ROW)
            {
                NSDate *dataUltimoTreino =[[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 0)] toDate] ;
                NSString *nomeGrupo =[[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 1)];
                
                
                NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] ;
                NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                                           fromDate:dataUltimoTreino
                                                             toDate:dataTreino
                                                            options:0];

                if(components.day  < intervalorG.intervalo && components.day && components.month==0){
                    int faltam = intervalorG.intervalo - (int)components.day;
                    retorno = [NSString stringWithFormat:@"%@ Faltam %i dias para recuperar o grupo muscular : %@\n",retorno , faltam, nomeGrupo];
                }
            }
        }
    }
    return retorno;
}
-(void)updateIntervalo:(int)intervalo {
    sqlite3 *db;
    sqlite3_stmt *stmt;
    db = [Database sharedDatabase];
    stmt = [Database sharedStatement];
    NSString *query =@"";
    query = [NSString stringWithFormat:
             @"UPDATE INTERVALO_TREINO  "
             " SET  "
             "     Intervalo = '%i' "
             "  WHERE CodAtleta = '%@' "
             "        AND CodPeriodo = '%@' "
             "        AND CodGrupoMuscular1 = '%@' "
             "        AND CodGrupoMuscular2 = '%@' "
             " ; "
             , intervalo
             ,[Parameters sharedParameters].atleta.code
             ,[Parameters sharedParameters].periodo.code
             , self.codGrupoMuscular1
             , self.codGrupoMuscular2
             ];
    char *errMsg = NULL;
    
    
    if (sqlite3_exec(db, "BEGIN", 0, 0, 0) == SQLITE_OK)
    {
        
        int execResult = sqlite3_exec(db, [query UTF8String], NULL, NULL, &errMsg);
        
        if (execResult != SQLITE_OK)
        {
            sqlite3_exec(db, "ROLLBACK", 0, 0, 0);
            
            NSString *errorMsg = [NSString stringWithFormat:
                                  @"Falha no sqlite3_exec na classe %@;\n"
                                  "método updateIntervalo;\n"
                                  "sqlite error code : %i;\n"
                                  "error msg: %s"
                                  "SQL: %@", self.class ,execResult, errMsg, query];
            ALog(@"%@", errorMsg);
        }
        
        sqlite3_exec(db, "COMMIT", 0, 0, 0);
    }
    else
    {
        NSString *errorMsg = [NSString stringWithFormat:
                              @"Falha no sqlite3_exec na classe %@;\n"
                              "método updateIntervalo;\n"
                              "BEGIN TRANSACTION ERROR", self.class];
        
        ALog(@"%@", errorMsg);
        
    }
}
@end
