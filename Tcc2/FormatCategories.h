//
//  FormatCategories.h
//  Tcc2
//
//  Created by A. J. on 06/09/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (FormatCategories)

- (NSDate *)toDate;


- (NSDate *)toDateWithFormat:(NSString *)format;

@end

@interface NSDate (FormatCategories)

- (NSString *)toUIString;

- (NSString *)toDbString;

- (NSString *)toStringWithFormat:(NSString *)format;

- (NSDate *)dateAtMidnight;
-(NSDate *)addDays:(int)days;
-(BOOL)isTheSameDayOf:(NSDate *)dia;
- (NSString *)toISO8601String;

- (BOOL) isDbNull;

@end

@interface NSNumber (FormatCategories)

- (NSString *)toUIString;


@end

@interface NSDecimalNumber (FormatCategories)

- (NSString *)toUIString;

@end
@interface UIColor (FormatCategories)
+(UIColor *)colorWithHexString:(NSString *)stringToConvert;
@end;
