//
//  DatabaseUtils.h
//  Tcc2
//
//  Created by Adolf Jurgens on 17/08/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.

#import <Foundation/Foundation.h>

/**
 Utilidades de banco de dados
 **/
@interface DatabaseUtils : NSObject

/* Caminho do banco */
+ (NSString *)dbPath;

/** Método para copia do banco do resources bundle p/ um local em que possa ser manipulado
 (arquivos no bundle não têm permissao de escrita **/
+ (void)copyDatabase;

+ (void)dropCurrentDatabase;

@end
