//
//  DiaSemana.m
//  Tcc2
//
//  Created by Adolf Jurgens on 17/08/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import "DiaSemana.h"

@implementation DiaSemana

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code

    }
    return self;
}


- (id)initWithDia:(NSDate*)data andOffSet:(int)offSet
{
        _arrayGrupos = [NSMutableArray new];    
    int w = 141;
    int h = 438;
    
    
    
    self = [super initWithFrame:CGRectMake(offSet, 61, w, h)];
    if (self) {
        _imgViewBG = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_dias"]];
        [_imgViewBG setFrame:CGRectMake(0, 0, 141, 438)];
        [self addSubview:_imgViewBG];
        
        _lblData = [[UILabel alloc] initWithFrame:CGRectMake(0, 7, w, 21)];
        _lblDiaSemana = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, w, 21)];
        _data = data;
        NSLocale *brLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init]; [dateFormat setDateFormat:@"cccc"];
        [dateFormat setLocale:brLocale];
        
       
        
        [_lblDiaSemana setText:[dateFormat stringFromDate:data]];
        [_lblDiaSemana setTextAlignment:NSTextAlignmentCenter];
       
        
        [dateFormat setDateFormat:@"dd/MM"];
        
        [_lblData setText:[dateFormat stringFromDate:data]];
        [_lblData setTextAlignment:NSTextAlignmentCenter];
        
        
        
        [self addSubview:_lblDiaSemana];
        [self addSubview:_lblData];
        
        //[self setBackgroundColor:[UIColor lightGrayColor]];

    }
    return self;
}

@end
