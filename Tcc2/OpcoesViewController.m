//
//  OpcoesViewController.m
//  Tcc2
//
//  Created by A. J. on 06/09/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import "OpcoesViewController.h"

@implementation OpcoesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _isFirstLoad = YES;
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"#efeff4"]];

}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

#pragma mark - Change_Modal_Size
- (void)viewWillLayoutSubviews{
    if(_isFirstLoad){
    [super viewWillLayoutSubviews];
    self.view.superview.bounds = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    self.view.superview.frame = CGRectMake(CGRectGetMidX(self.view.superview.frame) -822/2 ,
                                           CGRectGetMidY(self.view.superview.frame)-575/2, 822 , 575);
        _isFirstLoad = NO;
    }
}
#pragma mark - Actions
- (IBAction)aAddAtleta:(id)sender {
    if(_pAtletaNome.text.length > 0){
        AtletaDB * atleta = [AtletaDB new];
        atleta.nome = [_pAtletaNome text];
        atleta.sexo = _pSwitchSexo.isOn ? @"F" : @"M";
        atleta.dataNascimento = [_pDataNascimento date];
        [atleta save];
        [_pAtletas addObject:atleta];
        [_pTableViewAtletas reloadData];
        [self limpaCampos];
    }
    else{
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Atenção" message:@"É necessário inserir o nome do atleta para adiciona-lo" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];

    }
}
- (IBAction)aAddPeriodo:(id)sender {
    if(_pPeriodoNome.text.length > 0){
        PeriodoDB * periodo = [PeriodoDB new];
        periodo.nome = [_pPeriodoNome text];
        [periodo save];
        [_pPeriodos addObject:periodo];
        [_pTableViewPeriodos reloadData];
        [self limpaCampos];
    }
    else{
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Atenção" message:@"É necessário inserir o nome do periodo para adiciona-lo" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}
- (IBAction)aOK:(id)sender {

    [self fecharTela];
}
-(void)fecharTela{
    [_mainVC setPPeriodos:_pPeriodos];
    [_mainVC setPAtletas:_pAtletas];
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Custom_Methods
-(void)limpaCampos{
    [_pAtletaNome setText:@""];
    [_pSwitchSexo setOn:YES];
    [_pDataNascimento setDate:[NSDate date]];
    [_pPeriodoNome setText:@""];
}
#pragma mark - TableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == _pTableViewAtletas){
        return _pAtletas.count;
    }else{
        return _pPeriodos.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell ;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    NSString *stringFromDate;

    if (tableView == _pTableViewAtletas) {
        static NSString *CellIdentifier = @"idAtletaCell";
        
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        // Configure the cell...
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                                        reuseIdentifier:CellIdentifier];
        }
        AtletaDB *atleta = [_pAtletas objectAtIndex:indexPath.row];
        stringFromDate = [formatter stringFromDate:atleta.dataNascimento];
        [(UILabel *)[cell viewWithTag:1]  setText:atleta.nome];
        [(UILabel *)[cell viewWithTag:2]  setText:atleta.sexo];
        [(UILabel *)[cell viewWithTag:3]  setText:stringFromDate];
        [cell setBackgroundColor:[UIColor clearColor]];
        
        return cell;
    }
    else{
        static NSString *CellIdentifier = @"idPeriodoCell";
        
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        // Configure the cell...
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                          reuseIdentifier:CellIdentifier];
        }
        PeriodoDB *periodo = [_pPeriodos objectAtIndex:indexPath.row];
        [(UILabel *)[cell viewWithTag:1]  setText:periodo.nome];
        [cell setBackgroundColor:[UIColor clearColor]];
        
     return cell;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView ==_pTableViewPeriodos)
        return 30;
    else{
        return 40;
    }
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        _indexDelete = indexPath.row;
        
        if(tableView == _pTableViewAtletas)
            _atletaDelete = [_pAtletas objectAtIndex:indexPath.row];
        else
            _periodoDelete = [_pPeriodos objectAtIndex:indexPath.row];

        _alertDelete =[[UIAlertView alloc] initWithTitle:@"Atenção"
                                                 message:[NSString stringWithFormat:@"Ao deletar um %@, todos os treinos ligado a ele tambem serão excluidos. Deseja mesmo fazer isso?",_atletaDelete?@"Atleta":@"Periodo"]
                                                delegate:self
                                       cancelButtonTitle:@"Não"
                                       otherButtonTitles:@"Sim", nil];
        [_alertDelete show];

    }
}
#pragma mark - AlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 0)
    {
        if (alertView == _alertDelete)
        {
            NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
            
            if(_atletaDelete && [_atletaDelete remove]){
                if([_atletaDelete.code isEqualToString:[Parameters sharedParameters].atleta.code]){
                    [Parameters sharedParameters].atleta = nil;
                    [notificationCenter postNotificationName:kNotificationReloadTreinos object:nil];
                }
                [_pAtletas removeObjectAtIndex:_indexDelete];
                [_pTableViewAtletas reloadData];

            }
            else if(_periodoDelete && [_periodoDelete remove]){
                if([_periodoDelete.code isEqualToString:[Parameters sharedParameters].periodo.code]){
                    [Parameters sharedParameters].periodo = nil;
                    [notificationCenter postNotificationName:kNotificationReloadTreinos object:nil];
                }
                [_pPeriodos removeObjectAtIndex:_indexDelete];
                [_pTableViewPeriodos reloadData];
            }
        }
    }
    _atletaDelete = nil;
    _periodoDelete = nil;
    _indexDelete = -1;
}



#pragma mark - KeyBoard_Methods
-(void)keyboardWillHide {
    if (self.view.frame.origin.y >= 0)
    {
        //[self setViewMovedUp:YES];
    }
    else if (self.view.frame.origin.y < 0)
    {
        [self setViewMovedUp:NO];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)sender
{
    if ([sender isEqual:_pPeriodoNome])
        if( self.view.frame.origin.y >= 0)
            [self setViewMovedUp:YES];
    
    self.currentResponder = sender;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([_pPeriodoNome isFirstResponder] && [touch view] != _pPeriodoNome) {
        [_pPeriodoNome resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    CGRect rect = self.view.frame;
    if (movedUp)
    {
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else
    {
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}


@end
