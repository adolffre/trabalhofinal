//
//  DiaSemana.h
//  Tcc2
//
//  Created by Adolf Jurgens on 17/08/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiaSemana : UIScrollView<UIGestureRecognizerDelegate>

@property(nonatomic, strong)UILabel *lblData;
@property(nonatomic, strong)UILabel *lblDiaSemana;
@property(nonatomic, strong)UIImageView *imgViewBG;
@property(nonatomic)NSInteger *countTreinos;
@property(nonatomic, strong)NSMutableArray * arrayGrupos;
@property(nonatomic, strong)NSDate *data;
- (id)initWithDia:(NSDate*)data andOffSet:(int)offSet;



@end
