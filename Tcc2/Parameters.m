//
//  Parameters.m
//  Tcc2
//
//  Created by A. J. on 28/09/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import "Parameters.h"

@implementation Parameters

static Parameters *_sharedInstance = nil;

+ (Parameters *)sharedParameters
{
    
    if (_sharedInstance == nil)
    {
        _sharedInstance = [[Parameters alloc] init];
    }
    
    return _sharedInstance;
}

- (id)init
{
    self = [super init];
    
    _atleta = nil;
    _periodo = nil;
    return self;
}

@end
