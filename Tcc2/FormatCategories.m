//
//  FormatCategories.m
//  Tcc2
//
//  Created by A. J. on 06/09/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import "FormatCategories.h"

@implementation NSString (FormatCategories)

- (NSDate *)toDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"]];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss +ssss";
    NSDate *date = [formatter dateFromString: self];
    
//    unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
//    NSCalendar* calendar = [NSCalendar currentCalendar];
//    NSDateComponents* components = [calendar components:flags fromDate:date];
//    //retorna apenas data com hora zerada
//    
    return  date;

}

- (NSDate *)toDateWithFormat:(NSString *)format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    formatter.dateFormat = [format stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    
    return [formatter dateFromString: [self stringByReplacingOccurrencesOfString:@"T" withString:@" "]];
}

@end

@implementation NSDate (FormatCategories)

- (NSString *)toUIString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    formatter.dateFormat = @"dd/MM/yyyy";
    
    return [formatter stringFromDate:self];
}

- (NSString *)toDbString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *stringDate = [dateFormatter stringFromDate:self];
    
    stringDate = [NSString stringWithFormat:@"%@ 00:00:00 +0000",stringDate];

    return stringDate;
}

- (NSString *)toStringWithFormat:(NSString *)format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    formatter.dateFormat = format;
    
    return [formatter stringFromDate:self];
}

- (NSString *)toISO8601String
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss";
        
    return [formatter stringFromDate:self];
}


- (NSDate *)dateAtMidnight
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *stringDate = [dateFormatter stringFromDate:self];
    
    stringDate = [NSString stringWithFormat:@"%@ 00:00:00",stringDate];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [dateFormatter dateFromString:stringDate];

    return dateFromString;
}
-(NSDate *)addDays:(int)days{
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSHourCalendarUnit | NSMinuteCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:self];
    
    //get nulled date
    [components setMinute:0];
    [components setHour:0];
    [components setSecond:0];
    
    NSDateComponents * day = [[NSDateComponents alloc] init];
    day.day = days;
    
    NSDate *dateTodayNulled = [[NSCalendar currentCalendar] dateFromComponents:components];
    
    NSDate *newDate = [[NSCalendar currentCalendar] dateByAddingComponents:day toDate:dateTodayNulled options:0];;
    
    return newDate;
}
//    NSDate *date = self;
//    unsigned int flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
//    NSCalendar* calendar = [NSCalendar currentCalendar];
//    NSDateComponents* components = [calendar components:flags fromDate:date];
//    //retorna apenas data com hora zerada
//    
//    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"BRT"];
////    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
////    
////    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
////    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
////    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
////    
////    NSDate* destinationDate = [[[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate] autorelease];
//    NSUInteger day1 = [[NSCalendar currentCalendar] ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:date];
//    NSUInteger day2 = [[NSCalendar currentCalendar] ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:[NSDate date]];
//    
//    NSDate* dateOnly = [calendar dateFromComponents:components];
//
//    return  [[calendar dateFromComponents:components] dateByAddingTimeInterval:    [sourceTimeZone secondsFromGMT]];
//
//}
-(BOOL)isTheSameDayOf:(NSDate *)dia{
    NSInteger day1 = [[NSCalendar currentCalendar] ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:self];
    NSInteger day2 = [[NSCalendar currentCalendar] ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:dia];
    
    return day1 ==day2;
}
- (BOOL) isDbNull
{
    if ([[self toDbString] isEqualToString: @"1900-01-01 00:00:00.000"]) {
        return YES;
    }
    
    return NO;
}

@end

@implementation NSNumber (FormatCategories)

- (NSString *)toUIString
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    numberFormatter.locale = [NSLocale currentLocale];
    return [numberFormatter stringFromNumber:self];
}

@end

@implementation NSDecimalNumber (FormatCategories)

- (NSString *)toUIString
{
    if ([self integerValue] == 0)
        return @"";
    
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    numberFormatter.locale = [NSLocale currentLocale];
    numberFormatter.minimumFractionDigits = 2;
    numberFormatter.maximumFractionDigits = 2;
    return [numberFormatter stringFromNumber:self];
//    return [NSString stringWithFormat:@"R$%@", [numberFormatter stringFromNumber:self]];
}

@end
@implementation UIColor (FormatCategories)

+(UIColor *)colorWithHexString:(NSString *)stringToConvert
{
    NSString *noHashString = [stringToConvert stringByReplacingOccurrencesOfString:@"#" withString:@""]; // remove the #
    NSScanner *scanner = [NSScanner scannerWithString:noHashString];
    [scanner setCharactersToBeSkipped:[NSCharacterSet symbolCharacterSet]]; // remove + and $
    
    unsigned hex;
    if (![scanner scanHexInt:&hex]) return nil;
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;
    
    return [UIColor colorWithRed:r / 255.0f green:g / 255.0f blue:b / 255.0f alpha:1.0f];
}
@end

