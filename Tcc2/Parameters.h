//
//  Parameters.h
//  Tcc2
//
//  Created by A. J. on 28/09/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AtletaDB.h"
#import "PeriodoDB.h"

@interface Parameters : NSObject
@property (nonatomic, strong) AtletaDB *atleta;
@property (nonatomic, strong) PeriodoDB *periodo;

+ (Parameters *)sharedParameters;

@end
