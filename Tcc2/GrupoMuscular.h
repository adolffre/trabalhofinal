//
//  GrupoMuscular.h
//  Tcc2
//
//  Created by Adolf Jurgens on 17/08/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GrupoMuscularDB.h"
#import "TreinoDB.h"
#import "FormatCategories.h"

@interface GrupoMuscular : UIScrollView
@property(nonatomic, strong)UILabel *lblGrupName;
@property(nonatomic, strong)NSString *codGrupo;
@property(nonatomic, strong)TreinoDB *treino;

- (id)initWithGrupo:(GrupoMuscularDB*)grupo andOffSet:(int)offSet;
- (id)initWithGrupo:(GrupoMuscularDB*)grupo andIndexDia:(int)index;
@end
