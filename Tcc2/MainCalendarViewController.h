//
//  MainCalendarViewController.h
//  Tcc2
//
//  Created by Adolf Jurgens on 17/08/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import "AtletaDB.h"
#import "DragDropManager.h"
#import "GrupoMuscular.h"
#import "DropDown.h"
#import "OpcoesViewController.h"
#import "PeriodoDB.h"
#import "GrupoMuscular.h"
#import "GrupoMuscularDB.h"
#import "DiaSemana.h"
#import "TreinoDB.h"
#import "Parameters.h"
#import "IntervaloTreinoDB.h"

#import <UIKit/UIKit.h>
@class OpcoesViewController;

@interface MainCalendarViewController : UIViewController<UIGestureRecognizerDelegate, dropDownDelegate, DragDropManagerDelegate>


@property(nonatomic, strong) AtletaDB *atletaSelecionado;
@property(nonatomic, strong) DiaSemana *pDiaSemanaAtual;
@property(nonatomic, strong) DragDropManager *dragDropManager;
@property(nonatomic, strong) GrupoMuscular *grupoBeingDragged;
@property(nonatomic, strong) GrupoMuscular *pGrupoMuscularAtual;
@property(nonatomic, strong) IBOutlet UIScrollView *viewGrupoPadrao;
@property(nonatomic, strong) NSArray *pGruposMusculares;
@property(nonatomic, strong) NSDate *dataUtilizada;
@property(nonatomic, strong) NSDictionary *pDictIntervalosGrupos;
@property(nonatomic, strong) NSMutableArray *draggableSubjects;
@property(nonatomic, strong) NSMutableArray *droppableAreas;
@property(nonatomic, strong) NSMutableArray *pAtletas;
@property(nonatomic, strong) NSMutableArray *pPeriodos;
@property(nonatomic, strong) NSMutableArray *pTreinosSemana;
@property(nonatomic, strong) OpcoesViewController *configVC;
@property(nonatomic, strong) PeriodoDB *periodoSelecionado;
@property(nonatomic, strong) TreinoDB *treinoDelete;
@property(nonatomic, strong) UIAlertView *alertDelete;
@property(nonatomic, strong) UIAlertView *alertLimiteTreino;
@property(nonatomic, weak) IBOutlet UIButton *pBtnAtletas;
@property(nonatomic, weak) IBOutlet UIButton *pBtnPeriodos;
@property(nonatomic, weak) IBOutlet UILabel *lblNomeGrupo;
@property(nonatomic, weak) IBOutlet UIScrollView *scrollDias;
@property(nonatomic, weak) IBOutlet UIScrollView *scrollGrupoMuscular;
@property(nonatomic, strong) UIScrollView *diaSelecionado;
@property (weak, nonatomic) IBOutlet UIScrollView *pScrollDetalhes;

- (IBAction)aBtnAtletas:(id)sender;
- (IBAction)aBtnPeriodos:(id)sender;
- (IBAction)aBtnDataAnterior:(id)sender;
- (IBAction)aBtnDataProxima:(id)sender;
- (void)aSelectButtonDrop:(UIButton *)sender;
-(NSString *)verificaDisponibilidadeGrupoMuscular:(NSString *)codGrupoM noDia:(NSDate *)dataSelecionada;
-(void)recarregaIntervaloTreino:(GrupoMuscular *)grupoMuscular;
@end
