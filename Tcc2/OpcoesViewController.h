//
//  OpcoesViewController.h
//  Tcc2
//
//  Created by A. J. on 06/09/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#define kOFFSET_FOR_KEYBOARD 80.0
#import <UIKit/UIKit.h>
#import "AtletaDB.h"
#import "PeriodoDB.h"
#import "MainCalendarViewController.h"

@class MainCalendarViewController;

@interface OpcoesViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *pAtletaNome;
@property (weak, nonatomic) IBOutlet UISwitch *pSwitchSexo;
@property (weak, nonatomic) IBOutlet UIDatePicker *pDataNascimento;
@property (weak, nonatomic) IBOutlet UITextField *pPeriodoNome;
@property (nonatomic) BOOL isFirstLoad;
@property (nonatomic, strong) NSMutableArray *pAtletas;
@property (nonatomic, strong) NSMutableArray *pPeriodos;
@property (nonatomic, strong) AtletaDB *atletaDelete;
@property (nonatomic, strong) PeriodoDB *periodoDelete;
@property (nonatomic, strong) id currentResponder;
@property (nonatomic) int indexDelete;

@property (nonatomic, strong) MainCalendarViewController *mainVC;

@property (nonatomic, strong) UIAlertView *alertDelete;



- (IBAction)aAddAtleta:(id)sender;
- (IBAction)aAddPeriodo:(id)sender;

- (IBAction)aOK:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *pTableViewAtletas;
@property (weak, nonatomic) IBOutlet UITableView *pTableViewPeriodos;

@end
