//
//  PeriodoDB.m
//  Tcc2
//
//  Created by A. J. on 06/09/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import "PeriodoDB.h"

@implementation PeriodoDB
- (id)init
{
    self = [super init];
    if (self) {
        _db = [Database sharedDatabase];
        _stmt = [Database sharedStatement];
    }
    return self;
}

- (void)save
{
    
    NSString *query = @"";
    self.code = [self createCodPeriodo];
    
    query = [NSString stringWithFormat:
             @"INSERT INTO PERIODO "
             " ( "
             "  CodPeriodo, "
             "  Nome "
             " ) "
             " values('%@', '%@') "
             " ; "
             , self.code
             , self.nome
             ];
    char *errMsg = NULL;
    
    
    if (sqlite3_exec(self.db, "BEGIN", 0, 0, 0) == SQLITE_OK)
    {
        
        int execResult = sqlite3_exec(self.db, [query UTF8String], NULL, NULL, &errMsg);
        
        if (execResult != SQLITE_OK)
        {
            sqlite3_exec(self.db, "ROLLBACK", 0, 0, 0);
            
            NSString *errorMsg = [NSString stringWithFormat:
                                  @"Falha no sqlite3_exec na classe %@;\n"
                                  "método save;\n"
                                  "sqlite error code : %i;\n"
                                  "error msg: %s"
                                  "SQL: %@", self.class ,execResult, errMsg, query];
            
            
            ALog(@"%@", errorMsg);
            
        }
        
        sqlite3_exec(self.db, "COMMIT", 0, 0, 0);
        
        [self createTableIntervaloForAtletasWithPeriodo:self];
        
    }
    else
    {
        NSString *errorMsg = [NSString stringWithFormat:
                              @"Falha no sqlite3_exec na classe %@;\n"
                              "método save;\n"
                              "BEGIN TRANSACTION ERROR", self.class];
        
        
        ALog(@"%@", errorMsg);
        
        
    }
}

- (NSString *)createCodPeriodo
{
    int ultimoCodPeriodo = 0;
    
    NSString *query = [[NSString alloc] initWithFormat:
                       @" SELECT MAX( "
                       "  CodPeriodo) "
                       "  FROM Periodo "];
    
    const char *query_stmt = [query UTF8String];
    
    if (sqlite3_prepare(_db, query_stmt, -1, &_stmt, NULL) == SQLITE_OK)
    {
        if (sqlite3_step(_stmt) == SQLITE_ROW)
        {
            ultimoCodPeriodo = sqlite3_column_int(_stmt, 0);
        }
    }
    
    return [NSString stringWithFormat:@"%i",ultimoCodPeriodo + 1];
}
+(NSMutableArray *)getPeriodos{
    
    sqlite3 *db;
    sqlite3_stmt *stmt;
    db = [Database sharedDatabase];
    stmt = [Database sharedStatement];
    NSMutableArray *periodos = [NSMutableArray new];
    NSString *query = [[NSString alloc] initWithFormat:
                       @" SELECT codPeriodo "
                       "    ,Nome "
                       "  FROM PERIODO "];
    
    const char *query_stmt = [query UTF8String];
    
    if (sqlite3_prepare(db, query_stmt, -1, &stmt, NULL) == SQLITE_OK)
    {
        while(sqlite3_step(stmt) == SQLITE_ROW)
        {
            PeriodoDB *periodo = [PeriodoDB new];
            
            periodo.code =  sqlite3_column_text(stmt, 0) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 0)] : nil;
            periodo.nome =  sqlite3_column_text(stmt, 1) ? [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(stmt, 1)] : nil;
            
            
            [periodos addObject:periodo];
        }
    }
    return  periodos;
    
}
- (BOOL)remove
{
    NSString *query = [NSString stringWithFormat:
                       @"DELETE FROM TREINO "
                       " WHERE CodPeriodo = '%@'; "
                       , self.code];
    
    
    query = [query stringByAppendingString:[NSString stringWithFormat:
                                            @"DELETE FROM PERIODO "
                                            " WHERE CodPeriodo = '%@'; "
                                            , self.code]];
    
    query = [query stringByAppendingString:[NSString stringWithFormat:
                                            @" DELETE FROM INTERVALO_TREINO "
                                            " WHERE CodAtleta = '%@'; "
                                            , self.code]];
    
    
    char *errMsg = NULL;
    
    if (sqlite3_exec(self.db, "BEGIN", 0, 0, 0) == SQLITE_OK)
    {
        
        int execResult = sqlite3_exec(self.db, [query UTF8String], NULL, NULL, &errMsg);
        
        if (execResult != SQLITE_OK)
        {
            sqlite3_exec(self.db, "ROLLBACK", 0, 0, 0);
            
            NSString *errorMsg = [NSString stringWithFormat:
                                  @"Falha no sqlite3_exec na classe %@;\n"
                                  "método remove;\n"
                                  "sqlite error code : %i;\n"
                                  "error msg: %s"
                                  "SQL: %@", self.class ,execResult, errMsg, query];
            
            
            ALog(@"%@", errorMsg);
            
            return NO;
        }
        
        sqlite3_exec(self.db, "COMMIT", 0, 0, 0);
    }
    else
    {
        NSString *errorMsg = [NSString stringWithFormat:
                              @"Falha no sqlite3_exec na classe %@;\n"
                              "método remove;\n"
                              "BEGIN TRANSACTION ERROR", self.class];
    
        ALog(@"%@", errorMsg);
        
        return YES;
    }
    
    return YES;
}

-(void)createTableIntervaloForAtletasWithPeriodo:(PeriodoDB *)periodo{
    
    NSString *query;
    
    NSArray *homens = [[NSArray alloc] initWithObjects:@"6",@"3",@"3",@"6",@"3",@"5",@"3",@"5",@"3",@"5",@"3",@"5",@"3",@"7",@"4", nil];
    NSArray *mulheres = [[NSArray alloc] initWithObjects:@"3",@"1",@"1",@"3",@"1",@"3",@"1",@"3",@"1",@"3",@"1",@"3",@"2",@"3",@"2", nil];
    NSArray *atletas = [AtletaDB getAtletas];
    
    for (AtletaDB *atleta in atletas) {
        NSArray *intervalos  = [atleta.sexo isEqualToString:@"M"]?homens:mulheres;
        int index = 0;
        NSString *codPediodo = periodo.code;
        
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"1",@"1",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"1",@"3",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"1",@"5",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"2",@"2",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"2",@"4",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"3",@"3",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"3",@"1",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"4",@"4",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"4",@"2",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"5",@"5",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"5",@"1",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"6",@"6",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"6",@"7",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"7",@"7",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
        index++;
        query = [NSString stringWithFormat:
                 @"INSERT INTO INTERVALO_TREINO ( codGrupoMuscular1, codGrupoMuscular2, codAtleta, codPeriodo, intervalo ) "
                 " values('%@', '%@', '%@', '%@', %@); ",@"7",@"6",atleta.code,codPediodo,[intervalos objectAtIndex:index]];
        [self runSQLCommand:query withErrorLog:NO];
    }
    
}

- (int) runSQLCommand:(NSString *)sqlCommand withErrorLog:(BOOL)withErrorLog
{
    NSString *sql;
    char * errMsg = NULL;
    
    int prepareResult = sqlite3_exec(_db, [sqlCommand UTF8String], NULL, NULL, &errMsg);
    
    if (prepareResult != SQLITE_OK)
    {
        NSString *errorMsg = [NSString stringWithFormat:
                              @"Falha no sqlite3_exec na classe %@;\n"
                              "método limpaDados;\n"
                              "sqlite error code : %i;\n"
                              "errMsg : %s;\n"
                              "SQL: %@", self.class ,prepareResult, errMsg , sql];
        
        if (withErrorLog)
            ALog(@"%@", errorMsg);
        
        return prepareResult;
    }
    
    return 0;
}


@end
