//
//  MainCalendarViewController.m
//  Tcc2
//
//  Created by Adolf Jurgens on 17/08/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import "MainCalendarViewController.h"

#define Grupos 7000

@implementation MainCalendarViewController{
    DropDown *dropDown;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recarregaTreinosPosDelete)
                                                 name:kNotificationReloadTreinos
                                               object:nil];

   
    [self inicializaPropertys];
    [self carregaGrupos];
    
    NSLog(@"app dir: %@",[[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
    [self.view setBackgroundColor:[UIColor colorWithHexString:@"#efeff4"]];
    
}

-(void)inicializaPropertys{
    _pAtletas = [AtletaDB getAtletas];
    _pPeriodos = [PeriodoDB getPeriodos];
    _pGruposMusculares = [GrupoMuscularDB getGruposMusculares];
    _pTreinosSemana = [NSMutableArray new];
    
    _draggableSubjects = [NSMutableArray new];
    _droppableAreas = [NSMutableArray new];
    _pBtnAtletas.layer.borderWidth = 1;
    _pBtnAtletas.layer.borderColor = [[UIColor blackColor] CGColor];
    _pBtnAtletas.layer.cornerRadius = 5;
    _pBtnPeriodos.layer.borderWidth = 1;
    _pBtnPeriodos.layer.borderColor = [[UIColor blackColor] CGColor];
    _pBtnPeriodos.layer.cornerRadius = 5;
    
    
    NSDate *hoje = [[NSDate new] dateAtMidnight];
    [self carregaDiasFromDia:hoje];
    _dragDropManager = [[DragDropManager alloc] initWithDragSubjects:_draggableSubjects andDropAreas:_droppableAreas];
    _dragDropManager.delegate = self;
    UIPanGestureRecognizer * uiTapGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:_dragDropManager action:@selector(dragging:)];
     [[self view] addGestureRecognizer:uiTapGestureRecognizer];
    [self recarregaDictIntervalosGrupos];

}

-(void)carregaGrupos{
    int offSetX =17;
    int index = 0;
    
    NSMutableArray *viewsToDelete = [NSMutableArray new];
    for (UIView *view in _scrollGrupoMuscular.subviews) {
        if([view isKindOfClass:[GrupoMuscular class]])
            [viewsToDelete addObject:view];
    }
    for (UIView *view in viewsToDelete) {
        
        [view removeFromSuperview];
    }
    [_draggableSubjects removeAllObjects];

    
    
    for (GrupoMuscularDB *grupo in _pGruposMusculares) {
        //imprime na tela os grupos musculares
        GrupoMuscular *scrollGrupo = [[GrupoMuscular alloc] initWithGrupo:grupo andOffSet:offSetX];
        scrollGrupo.tag =index+ Grupos;
        [_scrollGrupoMuscular addSubview:scrollGrupo];
        
        [_draggableSubjects addObject:scrollGrupo];
        offSetX+= 135 + 7;
        index++;
    }
}

-(void)carregaDiasFromDia:(NSDate *)data{
    int offSetX =6;
    int dias = 1;

    BOOL domingo = NO;

    while (!domingo) {
        
        //volta os dias até o achar o primeiro domingo
        
        NSLocale *brLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"pt_BR"];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init]; [dateFormat setDateFormat:@"cccc"];
        [dateFormat setLocale:brLocale];
        if(![[dateFormat stringFromDate:data] isEqualToString:@"domingo"]){
            data = [data addDays:-1];
        }
        else
            domingo = YES;
        
        
        _dataUtilizada = data;
    }
    while (dias <=7) {
        //imprime na tela os dias da semana
        DiaSemana *scrollDia = [[DiaSemana alloc] initWithDia:data andOffSet:offSetX];
        
        if (_pTreinosSemana.count) {
            int indexDia = 0;
            for (TreinoDB *treino in _pTreinosSemana) {
                if([[treino.dataTreino toDbString] isEqualToString: [data toDbString] ]){
                    for (GrupoMuscularDB *grupo in _pGruposMusculares) {
                        if ([grupo.code isEqualToString:treino.codGrupoMuscular]) {
                            GrupoMuscular *scrollGrupo = [[GrupoMuscular alloc] initWithGrupo:grupo andIndexDia:indexDia];
                            [scrollGrupo setTreino:treino];
                            [scrollDia addSubview:scrollGrupo];
                            [scrollDia.arrayGrupos addObject:grupo];

                            UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDelete:)];
                            [recognizer setNumberOfTapsRequired:1];
                            [scrollGrupo addGestureRecognizer:recognizer];
                            indexDia++;
                        }
                    }
                }
            }
        }
        data = [data addDays:1];
        offSetX+= 141 + 4;
        
        dias++;
        
        [_scrollDias addSubview:scrollDia];
        [_droppableAreas addObject:scrollDia];
    }
    
}
- (void)tapDelete:(UITapGestureRecognizer *)gestureRecognizer {
    UIView* view = gestureRecognizer.view;
    CGPoint loc = [gestureRecognizer locationInView:view];
    GrupoMuscular * grupoView = (GrupoMuscular *)[view hitTest:loc withEvent:nil];
    _treinoDelete = (TreinoDB *)grupoView.treino;
    _alertDelete =[[UIAlertView alloc] initWithTitle:@"Atenção"
                                             message:[NSString stringWithFormat:@"Deseja mesmo deletar este treino?"]
                                            delegate:self
                                   cancelButtonTitle:@"Não"
                                   otherButtonTitles:@"Sim", nil];
    [_alertDelete show];


}
-(NSString *)verificaDisponibilidadeGrupoMuscular:(NSString *)codGrupoM noDia:(NSDate *)dataSelecionada{
    NSArray * verificar = [_pDictIntervalosGrupos objectForKey:codGrupoM];
    NSString *possibilide = [IntervaloTreinoDB validaPossibilidadeTreino:verificar eDataTreino:dataSelecionada];
    return ![possibilide isEqualToString:@""]?possibilide:nil;
}
#pragma mark - DragDropManagerDelegate
-(void)dropGrupo:(UIView*)grupoMuscular onDia:(UIView *)diaSemana{

    if([(DiaSemana *)diaSemana arrayGrupos].count>=4){
        return;
    }
    NSString * possibilidade = [self verificaDisponibilidadeGrupoMuscular:[(GrupoMuscular *)grupoMuscular codGrupo] noDia: [(DiaSemana*)diaSemana data]];
    
    
    
    if (possibilidade ) {
        _pDiaSemanaAtual = (DiaSemana *)diaSemana;
        _pGrupoMuscularAtual = (GrupoMuscular *)grupoMuscular;
        _alertLimiteTreino =[[UIAlertView alloc] initWithTitle:@"Atenção"
                                                       message:[NSString stringWithFormat:@"%@\nDeseja adicionar o treino assim mesmo?",possibilidade]
                                                      delegate:self
                                             cancelButtonTitle:@"Não"
                                             otherButtonTitles:@"Sim", nil];
        [_alertLimiteTreino show];
        
    }
    else{
        [self adicionaTreino:(GrupoMuscular *)grupoMuscular onDia:(DiaSemana *)diaSemana];
        [self recarregaIntervaloTreino:(GrupoMuscular *)grupoMuscular];
    }
    
}
-(void)adicionaTreino:(GrupoMuscular *)grupoMuscular onDia:(DiaSemana*)diaSemana{
    BOOL jaTreinou = NO;
    for (UIView *dia in _scrollDias.subviews) {
        if ([dia isKindOfClass:[DiaSemana class]]) {
            if([[(DiaSemana *)dia data]isTheSameDayOf:[(DiaSemana*)diaSemana data]]){
                for (GrupoMuscularDB *grup in [(DiaSemana*)dia arrayGrupos]) {
                    if([grup.code isEqualToString:[(GrupoMuscular *)grupoMuscular codGrupo]]){
                        jaTreinou = YES;
                        break;
                    }
                }
                break;
            }
        }
        if(jaTreinou){
            [grupoMuscular removeFromSuperview];
            break;
        }
    }
    if (!jaTreinou) {
        TreinoDB *novoTreino = [TreinoDB new];
        [novoTreino setCodAtleta:_atletaSelecionado.code];
        [novoTreino setCodPeriodo:_periodoSelecionado.code];
        [novoTreino setCodGrupoMuscular:[(GrupoMuscular *)grupoMuscular codGrupo]];
        [novoTreino setDataTreino:[(DiaSemana *)diaSemana data] ];
        [novoTreino save];
        
    }
}
-(void)didFinishedDragDrop{
    [self carregaGrupos];
    [self recarregaTreinos];
}


#pragma mark - AlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 0)
    {
        if (alertView == _alertDelete)
        {
            [_treinoDelete remove];
            [self recarregaTreinos];
        }
        else if(alertView == _alertLimiteTreino){
            [self adicionaTreino:_pGrupoMuscularAtual onDia:_pDiaSemanaAtual];
            [self recarregaIntervaloTreino:_pGrupoMuscularAtual];
            _pDiaSemanaAtual = nil;
            _pGrupoMuscularAtual = nil;
            [self didFinishedDragDrop];
        }
    }
    _treinoDelete = nil;
}

-(void)limpaScrollDias{
    NSMutableArray *viewsToDelete = [NSMutableArray new];
    for (UIView *views in _scrollDias.subviews) {
        if([views isKindOfClass:[DiaSemana class]])
            [viewsToDelete addObject:views];
    }
    for (UIView *views in viewsToDelete) {
        
        [views removeFromSuperview];
    }
    [_droppableAreas removeAllObjects];
}
- (IBAction)aBtnAtletas:(id)sender {
    [self aSelectButtonDrop:(UIButton *)sender];
}

- (IBAction)aBtnPeriodos:(id)sender {
    [self aSelectButtonDrop:(UIButton *)sender];
}

- (IBAction)aBtnDataAnterior:(id)sender {

    _dataUtilizada =     [_dataUtilizada addDays:-7] ;
    [self recarregaTreinos];

}

- (IBAction)aBtnDataProxima:(id)sender {
    
    _dataUtilizada =   [_dataUtilizada addDays:7] ;
    [self recarregaTreinos];
}

- (void)aSelectButtonDrop:(UIButton *)sender {
    NSArray *arrayDrop;
    if(sender.tag == 1 && _pAtletas.count){
        //atletas
        
        arrayDrop = [NSArray arrayWithArray:_pAtletas];
    }
    else if(sender.tag ==2 && _pPeriodos.count){
        //periodos
        arrayDrop = [NSArray arrayWithArray:_pPeriodos];
    }
    
    if(dropDown == nil && arrayDrop.count) {
        dropDown = [[DropDown alloc] showDropDown:sender withArray:arrayDrop andDirection:@"down"];

        dropDown.delegate = self;
    }
    else if(dropDown!=nil){
        [dropDown hideDropDown:sender];
        dropDown = nil;
    }
}
- (void) dropDownDelegateMethod: (DropDown *)sender andIndex:(int)index{
    if(sender.tag==1){
        _atletaSelecionado = [_pAtletas objectAtIndex:index];
        [Parameters sharedParameters].atleta = _atletaSelecionado;
    }
    else{
        _periodoSelecionado = [_pPeriodos objectAtIndex:index];
        [Parameters sharedParameters].periodo = _periodoSelecionado;
    }
    if (_periodoSelecionado && _atletaSelecionado) {
        [self recarregaTreinos];
        [self recarregaDictIntervalosGrupos];
    }
    dropDown = nil;
}
-(void)recarregaTreinos{
    [self limpaScrollDias];
    _pTreinosSemana = [TreinoDB getTreinosFromAtleta:_atletaSelecionado inPeriodo:_periodoSelecionado atSemana:_dataUtilizada];
    [self carregaDiasFromDia:_dataUtilizada];

}
-(void)recarregaTreinosPosDelete{
    if(![_atletaSelecionado.code isEqualToString:[Parameters sharedParameters].atleta.code] ||[_periodoSelecionado.code isEqualToString:[Parameters sharedParameters].periodo.code]){
        _atletaSelecionado = nil;
        _periodoSelecionado = nil;
        [_pBtnAtletas setTitle:@"Selecione um(a) Atleta" forState:UIControlStateNormal];
        [_pBtnPeriodos setTitle:@"Selecione um Periodo" forState:UIControlStateNormal];
    }
    
    [self limpaScrollDias];
    _pTreinosSemana = [TreinoDB getTreinosFromAtleta:_atletaSelecionado inPeriodo:_periodoSelecionado atSemana:_dataUtilizada];
    [self carregaDiasFromDia:_dataUtilizada];
}
-(void)recarregaIntervaloTreino:(GrupoMuscular *)grupoMuscular{
    NSArray * intervalosGrupoSelecionado = [_pDictIntervalosGrupos objectForKey:grupoMuscular.codGrupo];

    for (int i =0; i<intervalosGrupoSelecionado.count; i++) {
        IntervaloTreinoDB *intervalorG = (IntervaloTreinoDB *)[intervalosGrupoSelecionado objectAtIndex:i];
        if(i==0){

            //intervalo treino 1
            
            NSArray *treinos = [TreinoDB getTreinosWithCodGrupoMuscular:intervalorG.codGrupoMuscular2];
            //treinos do segundo grupo
            
            
            int index = 0;
            int total = 0;
            if(treinos.count>3){
                while (index<treinos.count-1) {
                    TreinoDB *treino1 = [treinos objectAtIndex:index];
                    TreinoDB *treino2 = [treinos objectAtIndex:index+1];
                    
                    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] ;
                    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                                               fromDate:treino1.dataTreino
                                                                 toDate:treino2.dataTreino
                                                                options:0];
                    total += components.day;
                    index++;
                    //NSLog(@"treino %i = %i dias",index ,components.day);
                }
                //NSLog(@"Total = %i / %i",total,treinos.count-1);
                float roundedValue =(float)total / ((float)treinos.count-1);
                NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                [formatter setMaximumFractionDigits:0];
                [formatter setRoundingMode: NSNumberFormatterRoundHalfEven];
                NSString * a =[formatter stringFromNumber:[NSNumber numberWithFloat:roundedValue]] ;;
                total =[a intValue];
                //NSLog(@"Invervalo de %@ / %@= %i",intervalorG.codGrupoMuscular1,intervalorG.codGrupoMuscular2,total);
                [intervalorG updateIntervalo:total];
            }
        }
        else{

            
            NSArray *treinosPrincipal = [TreinoDB getTreinosWithCodGrupoMuscular:intervalorG.codGrupoMuscular1];
            NSArray *treinosSecundarios =[TreinoDB getTreinosWithCodGrupoMuscular:intervalorG.codGrupoMuscular2];
            if(treinosPrincipal.count>3){
            
                int index = 0;
                int total = 0;
                int isOk = YES;
                for (TreinoDB *treino2 in treinosSecundarios ) {
                    index = 0;
                    isOk = YES;
                    for (TreinoDB *treino1 in treinosPrincipal) {
                        
                        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] ;
                        NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                                                   fromDate:treino1.dataTreino
                                                                     toDate:treino2.dataTreino
                                                                    options:0];
                       
                        if((components.day <0 || index==treinosPrincipal.count )&& isOk){
                            if(index!= 0 ){
                        
                                TreinoDB *treinoMenor = [treinosPrincipal objectAtIndex:index-1];
                                components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                                                           fromDate:treinoMenor.dataTreino
                                                                             toDate:treino2.dataTreino
                                                                            options:0];
                                total +=components.day;
                            }
                            isOk = NO;
                        }
                        index++;
                        
                    }
                }
                float roundedValue =(float)total / ((float)treinosSecundarios.count);
                NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                [formatter setMaximumFractionDigits:0];
                [formatter setRoundingMode: NSNumberFormatterRoundHalfEven];
                NSString * a =[formatter stringFromNumber:[NSNumber numberWithFloat:roundedValue]] ;;
                total =[a intValue];
                
                //NSLog(@"Invervalo de %@ / %@= %i",intervalorG.codGrupoMuscular1,intervalorG.codGrupoMuscular2,total);
                [intervalorG updateIntervalo:total];
            }
        }
    }

        [self recarregaDictIntervalosGrupos];
    
}
-(void)recarregaDictIntervalosGrupos{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    
    NSMutableArray *keys  = [NSMutableArray new];
    if(_atletaSelecionado && _periodoSelecionado){
        int tag = 1;
        for (int i =1; i<=7; i++) {
            [keys addObject:[NSString stringWithFormat:@"%i",i]];
            
            NSArray *objects = [IntervaloTreinoDB intervalosFromCodGrupoMuscular:[NSString stringWithFormat:@"%i",i]withAtleta:_atletaSelecionado andPeriodo:_periodoSelecionado];
            [dic setObject:objects forKey:[NSString stringWithFormat:@"%i",i]];
            for (IntervaloTreinoDB *intervalo in objects) {
                UILabel *descri = (UILabel *)[_pScrollDetalhes viewWithTag:tag];
                [descri setText:[NSString stringWithFormat:@"%i",intervalo.intervalo]];
                tag++;
            }
        }
        _pDictIntervalosGrupos = [NSDictionary dictionaryWithDictionary:dic];
    }
}


#pragma mark - Segues

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"idConfiguracoes"])
    {
        self.configVC = segue.destinationViewController;
        [_configVC setPAtletas:_pAtletas];
        [_configVC setPPeriodos:_pPeriodos];
        [_configVC setMainVC:self];
    }
    
}

@end
