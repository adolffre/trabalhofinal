//
//  IntervaloTreinoDB.h
//  Tcc2
//
//  Created by A. J. on 28/09/14.
//  Copyright (c) 2014 Adolf Jurgens & Douglas Galisteo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "Database.h"
#import "AtletaDB.h"
#import "GrupoMuscularDB.h"
#import "PeriodoDB.h"
#import "Parameters.h"

@interface IntervaloTreinoDB : NSObject
@property (nonatomic, strong) NSString *codGrupoMuscular1;
@property (nonatomic, strong) NSString *codGrupoMuscular2;
@property (nonatomic, strong) NSString *codAtleta;
@property (nonatomic, strong) NSString *codPeriodo;
@property (nonatomic) int intervalo;

+(NSArray *)intervalosFromCodGrupoMuscular:(NSString *)codGrupoMuscular withAtleta: (AtletaDB *)atleta andPeriodo:(PeriodoDB *)periodo;
+(NSString *)validaPossibilidadeTreino:(NSArray *)intervalosGrupoSelecionado eDataTreino:(NSDate *)dataTreino;
-(void)updateIntervalo:(int)intervalo;

@end
